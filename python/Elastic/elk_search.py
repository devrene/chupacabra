#!/usr/bin/env python
# coding: utf-8

# In[2]:


from elasticsearch import Elasticsearch


# In[3]:


es = Elasticsearch([{'host': 'z-dev.ddns.net', 'port': 8193}])
#https://github.com/elastic/elasticsearch-py/issues/508


# In[4]:


def search_like (produto):
    response = es.search(
        index="produto",
        body={
          "query": {
            "more_like_this" : {
                "fields" : ["Produto", "Categoria"],
                "like" : produto,
                "min_term_freq" : 1,
                "max_query_terms" : 12
                }
            }      
        }
    )    
    return response['hits']['hits']


# In[14]:


def search_fuzzy (produto):
    response = es.search(
        index="produto",
        body = {"query": {
                    "fuzzy": {
                      "Produto": {
                        "value": produto
              }
            }
          }
        }
    )
    return response['hits']['hits']


# In[13]:


def search_match (produto):
    response = es.search(
        index="produto",
        body = {"query": {
        "match": {
          "Produto": {
            "query": produto,
            "operator": "and"
              }
            }
          }
        }
    )
    return response['hits']['hits']


# In[26]:


search_match('aroz')


# In[25]:


search_fuzzy('aroz')


# In[27]:


search_like('aroz')


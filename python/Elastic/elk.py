#!/usr/bin/env python
# coding: utf-8

# In[2]:


from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
import pyrebase
import json


# In[9]:


es = Elasticsearch([{'host': 'z-dev.ddns.net', 'port': 8193}])
#https://github.com/elastic/elasticsearch-py/issues/508


# In[3]:


config = {
    "apiKey": "AIzaSyBuTERX6biNebxht4q6sQ5ff1FolDN-VPg",
    "authDomain": "chupacabra-f0074.firebaseapp.com",
    "databaseURL": "https://chupacabra-f0074.firebaseio.com",
    "projectId": "chupacabra-f0074",
    "storageBucket": "chupacabra-f0074.appspot.com",
    "messagingSenderId": "627737838746"
  };

firebase = pyrebase.initialize_app(config)
db = firebase.database()

produtos = db.child("produtos").get()


# In[5]:


produto


# In[8]:


ACTIONS = []
count = 0
for produto in produtos.val():
    action = {
        "_index": "produto",
        "_type": "br",
        "_source":produto

    }
    ACTIONS.append(action) 

success, _ = bulk(es, ACTIONS, index = "produto", raise_on_error=True)
count += success
print("insert %s lines" % count)


# In[8]:


response = es.search(
    index="produto",
    body={
      "query": {
        "more_like_this" : {
            "fields" : ["Produto", "Categoria"],
            "like" : "ar",
            "min_term_freq" : 1,
            "max_query_terms" : 12
            }
        }      
    }
)

for hit in response['hits']['hits']:
    print(hit['_score'], hit['_source']['Produto'])


# In[13]:


response = es.search(
    index="produto",
    body = {"query": {
                "fuzzy": {
                  "Produto": {
                    "value": "parati"
          }
        }
      }
    }
)

for hit in response['hits']['hits']:
    print(hit['_score'], hit['_source']['Produto'])


# In[15]:


response = es.search(
    index="produto",
    body = {"query": {
    "match": {
      "Produto": {
        "query": "parati",
        "operator": "and"
          }
        }
      }
    }
)

for hit in response['hits']['hits']:
    print(hit['_score'], hit['_source']['Produto'])


# In[8]:


response['hits']['hits']


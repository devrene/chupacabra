import connexion

from flask import (
    Flask
)

app = connexion.App(__name__, specification_dir='./', options={"swagger_ui":True})
app.add_api('swagger.yaml')

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)

from elasticsearch import Elasticsearch
import os

elkHost = os.getenv('ELASTIC_HOST', default = 'localhost')
elkPort = os.getenv('ELASTIC_PORT', default = 9300)

es = Elasticsearch([{'host': elkHost, 'port': elkPort}])

def search(query):
    if (not es.ping()):
      return []
    response = es.search(
        index="produto",
        body = {"query": {
                    "fuzzy": {
                      "Produto": {
                        "value": query
              }
            }
          }
        }
    )
    return response['hits']['hits']

USE [ChupaCabra]
GO

/****** Object:  Table [dbo].[Produto]    Script Date: 13/02/2019 23:07:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Produto](
	["idCategory"] [varchar](5) NULL,
	["is_favorite"] [varchar](7) NULL,
	["description"] [varchar](151) NULL,
	["prices"] [varchar](96) NULL,
	["images"] [varchar](251) NULL,
	["isWeightVariable"] [varchar](7) NULL,
	["isRemoval"] [varchar](6) NULL,
	["quantityMinimumPromotion"] [varchar](3) NULL,
	["totalValue"] [varchar](5) NULL,
	["departmentUrl"] [varchar](9) NULL,
	["idDepartament"] [varchar](4) NULL,
	["relevance"] [varchar](4) NULL,
	["quantityStock"] [varchar](5) NULL,
	["brandUrl"] [varchar](14) NULL,
	["excerpt"] [varchar](58) NULL,
	["priceExclusive"] [varchar](7) NULL,
	["is_notification"] [varchar](7) NULL,
	["brand"] [varchar](14) NULL,
	["idLojaProduto"] [varchar](10) NULL,
	["ignorePromotion"] [varchar](7) NULL,
	["discount"] [varchar](25) NULL,
	["department"] [varchar](9) NULL,
	["image"] [varchar](109) NULL,
	["video"] [varchar](2) NULL,
	["imageFull"] [varchar](109) NULL,
	["quantity"] [varchar](3) NULL,
	["isSale"] [varchar](6) NULL,
	["isRupture"] [varchar](7) NULL,
	["title"] [varchar](14) NULL,
	["isDelivery"] [varchar](6) NULL,
	["idProduct"] [varchar](8) NULL,
	["observableType"] [varchar](3) NULL,
	["weight"] [varchar](7) NULL,
	["subCategoryUrl"] [varchar](29) NULL,
	["category"] [varchar](20) NULL,
	["isAds"] [varchar](7) NULL,
	["imageError"] [varchar](56) NULL,
	["categoryUrl"] [varchar](20) NULL,
	["idSubCategory"] [varchar](5) NULL,
	["price_old"] [varchar](5) NULL,
	["url"] [varchar](58) NULL,
	["subCategory"] [varchar](29) NULL,
	["ads"] [varchar](2) NULL,
	["unit"] [varchar](4) NULL,
	["dt_preco"] [varchar](12) NULL,
	["preco"] [varchar](7) NULL
) ON [PRIMARY]
GO



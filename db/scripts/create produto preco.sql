USE [ChupaCabra]
GO

/****** Object:  Table [dbo].[ProdutoPreco]    Script Date: 13/02/2019 23:08:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProdutoPreco](
	[Produto] [varchar](200) NULL,
	[Departamento] [varchar](50) NULL,
	[Imagem] [varchar](500) NULL,
	[CodProduto] [int] NULL,
	[PrecoMinimo] [numeric](18, 2) NULL,
	[PrecoMaximo] [numeric](18, 2) NULL,
	[PrecoMedio] [numeric](18, 2) NULL,
	[PrecoAtual] [numeric](18, 2) NULL,
	[Data Atualização] [date] NULL
) ON [PRIMARY]
GO



USE [ChupaCabra]
GO

/****** Object:  Table [dbo].[Produto]    Script Date: 20/02/2019 12:18:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Produto](
	["idProduto"] [varchar](50) NULL,
	["produto"] [varchar](200) NULL,
	["departamento"] [varchar](50) NULL,
	["imagem"] [varchar](500) NULL,
	["preco"] [varchar](50) NULL,
	["data"] [varchar](50) NULL
) ON [PRIMARY]
GO


